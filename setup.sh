# Modo de usar o script de instalação do GLPI
# bash glpi-install/setup.sh  servicedesk.empresa.com.br senha_DB_123 versao 

source glpi-install/.env

FQDN=$1
echo $FQDN
echo $DB_PASSWD
echo $GLPI_VERSION
echo $GLPI_VERSION


# Atualizando o fuso horário
timedatectl set-timezone America/Sao_Paulo

# Apache
apt install php apache2 libapache2-mod-php -y
# Banco de  Dados
apt install mariadb-server -y

# PHP
apt install php-mbstring \
php-curl \
php-gd \
php-intl \
php-ldap \
php-apcu \
php-xmlrpc \
php-zip \
php-bz2 \
php-cas \
php-simplexml \
php-mysqli -y

# Sincronização realtime
apt install lsyncd \
openssh-server -y

# Utilitários
apt install unzip  \
wget \
htop \
rpl \
zip \
nano \
ncdu \
curl \
gettext \
mycli  -y


# Dependencias FusionInventory
apt-get install  dmidecode \
ucf \
nmap \
hwdata \
hdparm \
libwww-perl \
libxml-treepp-perl \
libuniversal-require-perl \
libtext-template-perl \
libsocket-getaddrinfo-perl \
libproc-pid-file-perl \
libproc-daemon-perl \
libparse-edid-perl \
libparallel-forkmanager-perl \
libnet-snmp-perl \
libnet-nbname-perl \
libnet-nbname-perl \
libnet-ip-perl \
libnet-cups-perl \
libfile-which-perl \
libfile-copy-recursive-perl \
libdigest-sha-perl \
libcrypt-des-perl \
libyaml-perl \
fusioninventory-agent-task-network \
fusioninventory-agent-task-esx \
fusioninventory-agent-task-deploy \
fusioninventory-agent -y

# Dependencias LetsEncript para acesso via ssl  https
apt install certbot python3-certbot-apache -y

  rm /var/www/html/index.html
# Obtendo o GLPI
  wget https://github.com/glpi-project/glpi/releases/download/"$GLPI_VERSION"/glpi-"$GLPI_VERSION".tgz
  tar xfz glpi-"$GLPI_VERSION".tgz
  mkdir /var/www/html/"$FQDN"
  mv glpi/* /var/www/html/"$FQDN"
  chown www-data. -Rf /var/www/html/"$FQDN"
  mkdir /var/lib/glpi -p  # Diretorio dados glpi
  mkdir /etc/glpi         # Diretório de configuração do GLPI
  mkdir -p /var/log/glpi  # Diretório de logs do GLPI
  chown www-data. -Rf /var/lib/glpi
  chown www-data. -Rf /var/log/glpi
  chown www-data. -Rf /etc/glpi/


cat <<EOF>   /var/www/html/"$FQDN"/inc/downstream.php
  <?php
    define('GLPI_CONFIG_DIR', '/etc/glpi/');

    if (file_exists(GLPI_CONFIG_DIR . '/local_define.php')) {
       require_once GLPI_CONFIG_DIR . '/local_define.php';
    }
EOF

# Definindo diretórios de armazenamento
cat <<EOF> /etc/glpi/local_define.php
    <?php
    define('GLPI_VAR_DIR', '/var/lib/glpi');
    define('GLPI_LOG_DIR', '/var/log/glpi');
EOF

# Enviando os arquivos para o diretório /var/lib/glpi
  mv /var/www/html/"$FQDN"/files/* /var/lib/glpi/
  systemctl enable --now  mariadb
  systemctl enable --now  apache2


# Criando  base de dados GLPI
  mysql -u root -e "create database glpi character set utf8";
  mysql -u root -e "create user 'glpi'@'localhost' identified by '$DB_PASSWD'";
  mysql -u root -e "grant all privileges on glpi.* to 'glpi'@'localhost' with grant option";


# Configurando timeZone
  mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
  mysql -u root -e  "grant select on mysql.time_zone_name to 'glpi'@'localhost';"
  mysql -u root -e   "flush privileges;"


# Criando  virtualhost
cat <<EOF> /etc/apache2/sites-available/$FQDN.conf

<VirtualHost *:80>
	ServerAdmin	webmaster@$FQDN
	DocumentRoot	/var/www/html/$FQDN
	ServerName	$FQDN
	ServerAlias	www.$FQDN
	ErrorLog	/error.log
	CustomLog	/access.log combined
	ErrorDocument	403 http://$FQDN/
  ErrorDocument	404 http://$FQDN/
</VirtualHost>



# Timeout: The number of seconds before receives and sends time out.

	Timeout 200

# KeepAlive: Whether or not to allow persistent connections (more than
# one request per connection). Set to "Off" to deactivate.

	KeepAlive On

# MaxKeepAliveRequests: The maximum number of requests to allow
# during a persistent connection. Set to 0 to allow an unlimited amount.
# We recommend you leave this number high, for maximum performance.

	MaxKeepAliveRequests 100

# KeepAliveTimeout: Number of seconds to wait for the next request from the
# same client on the same connection.

	KeepAliveTimeout 60

# prefork MPM
# StartServers: number of server processes to start
# MinSpareServers: minimum number of server processes which are kept spare
# MaxSpareServers: maximum number of server processes which are kept spare
# ServerLimit: maximum value for MaxClients for the lifetime of the server
# MaxClients: maximum number of server processes allowed to start
# MaxRequestsPerChild: maximum number of requests a server process serves

<IfModule prefork.c>
	StartServers		16
	MinSpareServers	10
	MaxSpareServers	20
	ServerLimit		256
	MaxClients		256
	MaxRequestsPerChild	4000
</IfModule>


<ifmodule mpm_prefork_module=>
	StartServers		2
	MinSpareServers	6
	MaxSpareServers	12
	MaxClients		48
	MaxRequestsPerChild	6000
</ifmodule> 

EOF

cat <<EOF> /etc/apache2/conf-available/$FQDN.conf

<Directory /var/www/html/>
	Options -Indexes -FollowSymLinks -ExecCGI -Includes
	AllowOverride None
	Require all granted
	ServerSignature Off
</Directory>


EOF

apachectl configtest

cat <<EOF>> /etc/apache2/apache2.conf
  ServerName 127.0.0.1
EOF

# Habilitando  o virtualhost
  cp glpi-install/php.ini  /etc/php/7.4/apache2/
  a2ensite "$FQDN".conf
  a2enconf "$FQDN".conf
  systemctl restart apache2


# Instalando o GLPI via bin console
  # Paramentros personalizados
  #cp glpi-install/php/empty_data.php  /var/www/html/"$FQDN"/install/empty_data.php
  #rpl "http://localhost/glpi" "http://$FQDN" /var/www/html/"$FQDN"/install/empty_data.php
  php /var/www/html/"$FQDN"/bin/console glpi:system:check_requirements
  php /var/www/html/"$FQDN"/bin/console db:install -u glpi -d glpi -p "$DB_PASSWD" -L pt_BR -vvv --force -n


  chown www-data. -Rf /var/log/glpi/php-errors.log
  chown www-data. -Rf /var/lib/glpi/_cache/*
  chown www-data. -Rf /var/lib/glpi/_cache/glpi_cache_*

# Atualizando tabelas
 # bash glpi-install/plugins.sh  $1
  #php /var/www/html/"$FQDN"/bin/console glpi:migration:timestamps -n
  #chown www-data. -Rf /var/lib/glpi/_plugins/datainjection/ 

# Instalando Rclone
  curl https://rclone.org/install.sh | sudo bash

# Criar usuario para backups
  useradd  -m backupadmin -G www-data,backup  -s /bin/bash
  mysql -u root -e "GRANT LOCK TABLES, SELECT ON *.* TO 'backupadmin'@'%' IDENTIFIED VIA unix_socket";

# Configurando o backup
  rpl "vhost" "$1" glpi-install/scripts/glpiBackup.sh
  cp glpi-install/scripts/glpiBackup.sh /opt
  mkdir -p /var/backups/glpi
  chown 770 -R /var/backups/glpi
  chown backupadmin.backup -Rvf /var/backups/glpi/
  mkdir -p /home/backupadmin/.config/rclone/
  chown backupadmin.backup -Rvf /home/backupadmin/.config/rclone/


# Instalando serviços systemd
  git clone https://gitlab.com/ferreirarocha/glpi-install.git /tmp/glpi-install
  sed -i 's/change_glpi/'$FQDN'/gI' /tmp/glpi-install/services/*
  cp /tmp/glpi-install/services/* /lib/systemd/system/


# Ativando serviços do systemd
  input="/tmp/glpi-install/config/systemdServicos.txt"
  while IFS= read -r line
  do
    systemctl enable --now "$line"
  done < "$input"

# Removendo arquivos de instalação
  rm -rf /tmp/glpi-install
  rm -rf glpi glpi-9.5.3.tgz  plugins/
  rm -rf /var/www/html/"$FQDN"/install/install.php
#certbot --apache -d $FQDN

# Enviando css personalizado para  formCreator e GLPI
#  rsync -Pav glpi-install/css/palettes/* /var/www/html/"$FQDN"/css/palettes/
#  rsync -Pav glpi-install/plugins/formcreator/* /var/www/html/"$FQDN"/plugins/formcreator/
#  systemctl restart apache2

# Iniciando agente Fusioninventory
#echo "127.0.0.1  $FQDN" >> /etc/hosts

#sed -i 's,#server = http://server.domain.com/glpi/plugins/fusioninventory/,server = http://'$FQDN'/plugins/fusioninventory/,g' /etc/fusioninventory/agent.cfg

#systemctl enable --now fusioninventory-agent

#fusioninventory-agent &


# Tela informativa de pós instalação

 # ip=$(hostname -I | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
  backupPosInstalcao=$(ls -X -1 /var/backups/glpi/)

echo  -e "--------------------------------------------------------------------------------------\n
  O sistema foi instalado para acessa-lo,
  Insira o seguinte endereço no navegador\n\n  $ip/$FQDN \n\n
  Recomendamos que configure o seguinte apontamento em seu servidor de DNS interno\n\n
  Nome = $FQDN \n  Target = $ip \n  Tipo = A \n  TLL = 14400 \n  Classe= IN \n\n
  
  Para teste local insira a seguinte entra no /etc/hosts para cliente linux  ou c:\windows\systemc32\drivers\etc\hosts para cliente windows

  $ip  $FQDN \n 

  Foi gerado um backup pós instalação em /var/backups/glpi/ \n
  Esse será o local padrão para os próximos arquivos.
  
  


\n--------------------------------------------------------------------------------------"
