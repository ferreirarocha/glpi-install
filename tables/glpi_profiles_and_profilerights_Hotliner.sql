-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (5,'Hotliner','central',0,3,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',1,0,0,0,NULL,'[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (29,5,'location',0),(62,5,'itilcategory',0),(78,5,'queuednotification',0),(124,5,'solutiontemplate',0),(137,5,'changevalidation',20),(168,5,'domain',0),(184,5,'projecttask',0),(280,5,'computer',0),(281,5,'monitor',0),(282,5,'software',0),(283,5,'networking',0),(284,5,'internet',0),(285,5,'printer',0),(286,5,'peripheral',0),(287,5,'cartridge',0),(288,5,'consumable',0),(289,5,'phone',0),(291,5,'contact_enterprise',0),(292,5,'document',0),(293,5,'contract',0),(294,5,'infocom',0),(295,5,'knowbase',10240),(296,5,'reservation',0),(297,5,'reports',0),(298,5,'dropdown',0),(299,5,'device',0),(300,5,'typedoc',0),(301,5,'link',0),(302,5,'config',0),(303,5,'rule_ticket',0),(304,5,'rule_import',0),(305,5,'rule_ldap',0),(306,5,'rule_softwarecategories',0),(307,5,'search_config',0),(310,5,'profile',0),(311,5,'user',1025),(312,5,'group',0),(313,5,'entity',0),(314,5,'transfer',0),(315,5,'logs',0),(316,5,'reminder_public',0),(317,5,'rssfeed_public',0),(318,5,'bookmark_public',0),(319,5,'backup',0),(320,5,'ticket',140295),(321,5,'followup',12295),(322,5,'task',8193),(324,5,'project',1151),(326,5,'planning',1),(327,5,'taskcategory',0),(328,5,'netpoint',0),(329,5,'statistic',1),(330,5,'password_update',1),(331,5,'show_group_hardware',0),(332,5,'rule_dictionnary_software',0),(333,5,'rule_dictionnary_dropdown',0),(334,5,'budget',0),(335,5,'notification',0),(336,5,'rule_mailcollector',0),(339,5,'calendar',0),(340,5,'slm',0),(341,5,'rule_dictionnary_printer',0),(342,5,'problem',1024),(345,5,'itiltemplate',0),(346,5,'ticketrecurrent',0),(347,5,'ticketcost',23),(348,5,'change',1054),(350,5,'ticketvalidation',3088),(408,5,'state',0),(482,5,'knowbasecategory',0),(566,5,'license',0),(574,5,'line',0),(582,5,'lineoperator',0),(590,5,'devicesimcard_pinpuk',0),(598,5,'certificate',0),(606,5,'datacenter',0),(615,5,'personalization',3),(622,5,'rule_asset',0),(630,5,'global_validation',0),(637,5,'cluster',0),(645,5,'externalevent',0),(653,5,'dashboard',0),(661,5,'appliance',0),(670,5,'plugin_fusioninventory_menu',0),(678,5,'plugin_fusioninventory_agent',0),(686,5,'plugin_fusioninventory_remotecontrol',0),(694,5,'plugin_fusioninventory_configuration',0),(702,5,'plugin_fusioninventory_task',0),(710,5,'plugin_fusioninventory_wol',0),(718,5,'plugin_fusioninventory_group',0),(726,5,'plugin_fusioninventory_collect',0),(734,5,'plugin_fusioninventory_iprange',0),(742,5,'plugin_fusioninventory_credential',0),(750,5,'plugin_fusioninventory_credentialip',0),(758,5,'plugin_fusioninventory_esx',0),(766,5,'plugin_fusioninventory_configsecurity',0),(774,5,'plugin_fusioninventory_networkequipment',0),(782,5,'plugin_fusioninventory_printer',0),(790,5,'plugin_fusioninventory_unmanaged',0),(798,5,'plugin_fusioninventory_importxml',0),(806,5,'plugin_fusioninventory_reportprinter',0),(814,5,'plugin_fusioninventory_reportnetworkequipment',0),(822,5,'plugin_fusioninventory_lock',0),(830,5,'plugin_fusioninventory_ruleimport',0),(838,5,'plugin_fusioninventory_ruleentity',0),(846,5,'plugin_fusioninventory_rulelocation',0),(854,5,'plugin_fusioninventory_blacklist',0),(862,5,'plugin_fusioninventory_rulecollect',0),(870,5,'plugin_fusioninventory_ignoredimportdevice',0),(878,5,'plugin_fusioninventory_package',0),(886,5,'plugin_fusioninventory_userinteractiontemplate',0),(894,5,'plugin_fusioninventory_deploymirror',0),(902,5,'plugin_fusioninventory_selfpackage',0),(912,5,'plugin_moreticket',0),(920,5,'plugin_moreticket_justification',0),(932,5,'plugin_pdf',0),(941,5,'plugin_satisfaction',0),(950,5,'plugin_tag_tag',0),(958,5,'plugin_tasklists',0),(966,5,'plugin_tasklists_see_all',0),(974,5,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:17:12
