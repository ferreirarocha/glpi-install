-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (7,'Supervisor','central',0,3,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',0,0,0,0,NULL,'[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (30,7,'domain',23),(55,7,'solutiontemplate',23),(112,7,'projecttask',1025),(186,7,'taskcategory',23),(187,7,'netpoint',23),(252,7,'project',1151),(277,7,'change',1151),(338,7,'state',23),(343,7,'knowbasecategory',23),(414,7,'itilcategory',23),(415,7,'location',23),(422,7,'computer',127),(423,7,'monitor',127),(424,7,'software',127),(425,7,'networking',127),(426,7,'internet',31),(427,7,'printer',127),(428,7,'peripheral',127),(429,7,'cartridge',127),(430,7,'consumable',127),(431,7,'phone',127),(433,7,'contact_enterprise',96),(434,7,'document',127),(435,7,'contract',96),(436,7,'infocom',0),(437,7,'knowbase',14359),(438,7,'reservation',1055),(439,7,'reports',1),(440,7,'dropdown',0),(441,7,'device',0),(442,7,'typedoc',0),(443,7,'link',0),(444,7,'config',0),(445,7,'rule_ticket',1047),(446,7,'rule_import',0),(447,7,'rule_ldap',0),(448,7,'rule_softwarecategories',0),(449,7,'search_config',0),(451,7,'profile',0),(452,7,'user',1055),(453,7,'group',1),(454,7,'entity',33),(455,7,'transfer',1),(456,7,'logs',1),(457,7,'reminder_public',23),(458,7,'rssfeed_public',23),(459,7,'bookmark_public',0),(460,7,'backup',0),(461,7,'ticket',261151),(462,7,'followup',15383),(463,7,'task',13329),(464,7,'queuednotification',0),(465,7,'planning',3073),(468,7,'statistic',1),(469,7,'password_update',1),(470,7,'show_group_hardware',0),(471,7,'rule_dictionnary_software',0),(472,7,'rule_dictionnary_dropdown',0),(473,7,'budget',96),(474,7,'notification',0),(475,7,'rule_mailcollector',23),(476,7,'changevalidation',1044),(478,7,'calendar',23),(479,7,'slm',23),(480,7,'rule_dictionnary_printer',0),(481,7,'problem',1151),(485,7,'itiltemplate',23),(486,7,'ticketrecurrent',1),(487,7,'ticketcost',23),(490,7,'ticketvalidation',15376),(568,7,'license',127),(576,7,'line',127),(584,7,'lineoperator',23),(592,7,'devicesimcard_pinpuk',3),(600,7,'certificate',127),(608,7,'datacenter',31),(617,7,'personalization',3),(624,7,'rule_asset',0),(632,7,'global_validation',0),(639,7,'cluster',31),(647,7,'externalevent',31),(655,7,'dashboard',0),(663,7,'appliance',31),(672,7,'plugin_fusioninventory_menu',0),(680,7,'plugin_fusioninventory_agent',0),(688,7,'plugin_fusioninventory_remotecontrol',0),(696,7,'plugin_fusioninventory_configuration',0),(704,7,'plugin_fusioninventory_task',0),(712,7,'plugin_fusioninventory_wol',0),(720,7,'plugin_fusioninventory_group',0),(728,7,'plugin_fusioninventory_collect',0),(736,7,'plugin_fusioninventory_iprange',0),(744,7,'plugin_fusioninventory_credential',0),(752,7,'plugin_fusioninventory_credentialip',0),(760,7,'plugin_fusioninventory_esx',0),(768,7,'plugin_fusioninventory_configsecurity',0),(776,7,'plugin_fusioninventory_networkequipment',0),(784,7,'plugin_fusioninventory_printer',0),(792,7,'plugin_fusioninventory_unmanaged',0),(800,7,'plugin_fusioninventory_importxml',0),(808,7,'plugin_fusioninventory_reportprinter',0),(816,7,'plugin_fusioninventory_reportnetworkequipment',0),(824,7,'plugin_fusioninventory_lock',0),(832,7,'plugin_fusioninventory_ruleimport',0),(840,7,'plugin_fusioninventory_ruleentity',0),(848,7,'plugin_fusioninventory_rulelocation',0),(856,7,'plugin_fusioninventory_blacklist',0),(864,7,'plugin_fusioninventory_rulecollect',0),(872,7,'plugin_fusioninventory_ignoredimportdevice',0),(880,7,'plugin_fusioninventory_package',0),(888,7,'plugin_fusioninventory_userinteractiontemplate',0),(896,7,'plugin_fusioninventory_deploymirror',0),(904,7,'plugin_fusioninventory_selfpackage',0),(914,7,'plugin_moreticket',0),(922,7,'plugin_moreticket_justification',0),(934,7,'plugin_pdf',0),(943,7,'plugin_satisfaction',0),(952,7,'plugin_tag_tag',0),(960,7,'plugin_tasklists',0),(968,7,'plugin_tasklists_see_all',0),(976,7,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:07:57
