-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `glpi_profiles`
--


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (4,'Super-Admin','central',0,3,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',0,0,0,0,NULL,'[-1]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (61,4,'knowbasecategory',23),(96,4,'location',23),(132,4,'itilcategory',23),(136,4,'changevalidation',1044),(149,4,'queuednotification',31),(183,4,'projecttask',1121),(197,4,'solutiontemplate',23),(210,4,'computer',255),(211,4,'monitor',255),(212,4,'software',255),(213,4,'networking',255),(214,4,'internet',159),(215,4,'printer',255),(216,4,'peripheral',255),(217,4,'cartridge',255),(218,4,'consumable',255),(219,4,'phone',255),(220,4,'contact_enterprise',255),(221,4,'document',255),(222,4,'contract',255),(223,4,'infocom',23),(224,4,'knowbase',15383),(225,4,'reservation',1055),(226,4,'reports',1),(227,4,'dropdown',23),(228,4,'device',23),(229,4,'typedoc',23),(230,4,'link',159),(231,4,'config',3),(232,4,'rule_ticket',1047),(233,4,'rule_import',23),(234,4,'rule_ldap',23),(235,4,'rule_softwarecategories',23),(236,4,'search_config',3072),(238,4,'domain',23),(239,4,'profile',23),(240,4,'user',7327),(241,4,'group',119),(242,4,'entity',3327),(243,4,'transfer',23),(244,4,'logs',1),(245,4,'reminder_public',159),(246,4,'rssfeed_public',159),(247,4,'bookmark_public',23),(248,4,'backup',1045),(249,4,'ticket',261151),(250,4,'followup',15383),(251,4,'task',13329),(255,4,'planning',3073),(258,4,'statistic',1),(259,4,'password_update',1),(260,4,'show_group_hardware',1),(261,4,'rule_dictionnary_software',23),(262,4,'rule_dictionnary_dropdown',23),(263,4,'budget',127),(264,4,'notification',23),(265,4,'rule_mailcollector',23),(268,4,'calendar',23),(269,4,'slm',23),(270,4,'rule_dictionnary_printer',23),(271,4,'problem',1151),(274,4,'itiltemplate',23),(275,4,'ticketrecurrent',23),(276,4,'ticketcost',23),(279,4,'ticketvalidation',15376),(323,4,'project',1151),(397,4,'taskcategory',23),(398,4,'netpoint',23),(407,4,'state',23),(420,4,'change',1151),(565,4,'license',255),(573,4,'line',255),(581,4,'lineoperator',23),(589,4,'devicesimcard_pinpuk',3),(597,4,'certificate',255),(605,4,'datacenter',31),(610,4,'rule_asset',1047),(614,4,'personalization',3),(629,4,'global_validation',0),(636,4,'cluster',31),(644,4,'externalevent',1055),(652,4,'dashboard',23),(660,4,'appliance',31),(665,4,'plugin_datainjection_model',31),(666,4,'plugin_datainjection_use',1),(669,4,'plugin_fusioninventory_menu',1),(677,4,'plugin_fusioninventory_agent',23),(685,4,'plugin_fusioninventory_remotecontrol',1),(693,4,'plugin_fusioninventory_configuration',3),(701,4,'plugin_fusioninventory_task',23),(709,4,'plugin_fusioninventory_wol',1),(717,4,'plugin_fusioninventory_group',23),(725,4,'plugin_fusioninventory_collect',23),(733,4,'plugin_fusioninventory_iprange',23),(741,4,'plugin_fusioninventory_credential',23),(749,4,'plugin_fusioninventory_credentialip',23),(757,4,'plugin_fusioninventory_esx',23),(765,4,'plugin_fusioninventory_configsecurity',31),(773,4,'plugin_fusioninventory_networkequipment',23),(781,4,'plugin_fusioninventory_printer',23),(789,4,'plugin_fusioninventory_unmanaged',31),(797,4,'plugin_fusioninventory_importxml',4),(805,4,'plugin_fusioninventory_reportprinter',1),(813,4,'plugin_fusioninventory_reportnetworkequipment',1),(821,4,'plugin_fusioninventory_lock',23),(829,4,'plugin_fusioninventory_ruleimport',23),(837,4,'plugin_fusioninventory_ruleentity',23),(845,4,'plugin_fusioninventory_rulelocation',23),(853,4,'plugin_fusioninventory_blacklist',23),(861,4,'plugin_fusioninventory_rulecollect',23),(869,4,'plugin_fusioninventory_ignoredimportdevice',23),(877,4,'plugin_fusioninventory_package',23),(885,4,'plugin_fusioninventory_userinteractiontemplate',23),(893,4,'plugin_fusioninventory_deploymirror',23),(901,4,'plugin_fusioninventory_selfpackage',1),(907,4,'plugin_metademands',31),(908,4,'plugin_metademands_requester',1),(926,4,'plugin_moreticket_justification',1),(927,4,'plugin_moreticket',127),(928,4,'plugin_moreticket_hide_task_duration',1),(937,4,'plugin_pdf',1),(946,4,'plugin_satisfaction',31),(949,4,'plugin_tag_tag',31),(979,4,'plugin_tasklists',127),(980,4,'plugin_tasklists_see_all',1),(981,4,'plugin_tasklists_config',1);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:16:49
