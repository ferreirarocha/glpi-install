-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (6,'Technician','central',0,3,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',0,0,0,0,NULL,'[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (11,6,'queuednotification',0),(66,6,'changevalidation',20),(97,6,'domain',0),(111,6,'projecttask',1025),(125,6,'solutiontemplate',0),(256,6,'taskcategory',0),(257,6,'netpoint',0),(325,6,'project',1151),(337,6,'state',0),(349,6,'change',1151),(351,6,'computer',127),(352,6,'monitor',127),(353,6,'software',127),(354,6,'networking',127),(355,6,'internet',31),(356,6,'printer',127),(357,6,'peripheral',127),(358,6,'cartridge',127),(359,6,'consumable',127),(360,6,'phone',127),(362,6,'contact_enterprise',96),(363,6,'document',127),(364,6,'contract',96),(365,6,'infocom',0),(366,6,'knowbase',14359),(367,6,'reservation',1055),(368,6,'reports',1),(369,6,'dropdown',0),(370,6,'device',0),(371,6,'typedoc',0),(372,6,'link',0),(373,6,'config',0),(374,6,'rule_ticket',0),(375,6,'rule_import',0),(376,6,'rule_ldap',0),(377,6,'rule_softwarecategories',0),(378,6,'search_config',0),(380,6,'profile',0),(381,6,'user',1055),(382,6,'group',1),(383,6,'entity',33),(384,6,'transfer',1),(385,6,'logs',0),(386,6,'reminder_public',23),(387,6,'rssfeed_public',23),(388,6,'bookmark_public',0),(389,6,'backup',0),(390,6,'ticket',166919),(391,6,'followup',13319),(392,6,'task',13329),(396,6,'planning',1),(399,6,'statistic',1),(400,6,'password_update',1),(401,6,'show_group_hardware',0),(402,6,'rule_dictionnary_software',0),(403,6,'rule_dictionnary_dropdown',0),(404,6,'budget',96),(405,6,'notification',0),(406,6,'rule_mailcollector',0),(409,6,'calendar',0),(410,6,'slm',1),(411,6,'rule_dictionnary_printer',0),(412,6,'problem',1121),(413,6,'knowbasecategory',0),(416,6,'itiltemplate',1),(417,6,'ticketrecurrent',1),(418,6,'ticketcost',23),(421,6,'ticketvalidation',3088),(483,6,'itilcategory',0),(484,6,'location',0),(567,6,'license',127),(575,6,'line',127),(583,6,'lineoperator',0),(591,6,'devicesimcard_pinpuk',3),(599,6,'certificate',127),(607,6,'datacenter',31),(616,6,'personalization',3),(623,6,'rule_asset',0),(631,6,'global_validation',0),(638,6,'cluster',31),(646,6,'externalevent',1),(654,6,'dashboard',0),(662,6,'appliance',31),(671,6,'plugin_fusioninventory_menu',0),(679,6,'plugin_fusioninventory_agent',0),(687,6,'plugin_fusioninventory_remotecontrol',0),(695,6,'plugin_fusioninventory_configuration',0),(703,6,'plugin_fusioninventory_task',0),(711,6,'plugin_fusioninventory_wol',0),(719,6,'plugin_fusioninventory_group',0),(727,6,'plugin_fusioninventory_collect',0),(735,6,'plugin_fusioninventory_iprange',0),(743,6,'plugin_fusioninventory_credential',0),(751,6,'plugin_fusioninventory_credentialip',0),(759,6,'plugin_fusioninventory_esx',0),(767,6,'plugin_fusioninventory_configsecurity',0),(775,6,'plugin_fusioninventory_networkequipment',0),(783,6,'plugin_fusioninventory_printer',0),(791,6,'plugin_fusioninventory_unmanaged',0),(799,6,'plugin_fusioninventory_importxml',0),(807,6,'plugin_fusioninventory_reportprinter',0),(815,6,'plugin_fusioninventory_reportnetworkequipment',0),(823,6,'plugin_fusioninventory_lock',0),(831,6,'plugin_fusioninventory_ruleimport',0),(839,6,'plugin_fusioninventory_ruleentity',0),(847,6,'plugin_fusioninventory_rulelocation',0),(855,6,'plugin_fusioninventory_blacklist',0),(863,6,'plugin_fusioninventory_rulecollect',0),(871,6,'plugin_fusioninventory_ignoredimportdevice',0),(879,6,'plugin_fusioninventory_package',0),(887,6,'plugin_fusioninventory_userinteractiontemplate',0),(895,6,'plugin_fusioninventory_deploymirror',0),(903,6,'plugin_fusioninventory_selfpackage',0),(913,6,'plugin_moreticket',0),(921,6,'plugin_moreticket_justification',0),(933,6,'plugin_pdf',0),(942,6,'plugin_satisfaction',0),(951,6,'plugin_tag_tag',0),(959,6,'plugin_tasklists',0),(967,6,'plugin_tasklists_see_all',0),(975,6,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:17:54
