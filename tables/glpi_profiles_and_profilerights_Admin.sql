-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (3,'Admin','central',0,3,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',0,0,0,0,NULL,'[-1]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glpi_profilerights`
--


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (131,3,'knowbasecategory',23),(139,3,'computer',127),(140,3,'monitor',127),(141,3,'software',127),(142,3,'networking',127),(143,3,'internet',31),(144,3,'printer',127),(145,3,'peripheral',127),(146,3,'cartridge',127),(147,3,'consumable',127),(148,3,'phone',127),(150,3,'contact_enterprise',127),(151,3,'document',127),(152,3,'contract',127),(153,3,'infocom',23),(154,3,'knowbase',14359),(155,3,'reservation',1055),(156,3,'reports',1),(157,3,'dropdown',23),(158,3,'device',23),(159,3,'typedoc',23),(160,3,'link',23),(161,3,'config',0),(162,3,'rule_ticket',1047),(163,3,'rule_import',0),(164,3,'rule_ldap',0),(165,3,'rule_softwarecategories',0),(166,3,'search_config',3072),(167,3,'location',23),(169,3,'profile',1),(170,3,'user',7199),(171,3,'group',119),(172,3,'entity',33),(173,3,'transfer',1),(174,3,'logs',1),(175,3,'reminder_public',23),(176,3,'rssfeed_public',23),(177,3,'bookmark_public',23),(178,3,'backup',1024),(179,3,'ticket',261151),(180,3,'followup',15383),(181,3,'task',13329),(182,3,'projecttask',1121),(185,3,'planning',3073),(188,3,'statistic',1),(189,3,'password_update',1),(190,3,'show_group_hardware',0),(191,3,'rule_dictionnary_software',0),(192,3,'rule_dictionnary_dropdown',0),(193,3,'budget',127),(194,3,'notification',0),(195,3,'rule_mailcollector',23),(196,3,'solutiontemplate',23),(198,3,'calendar',23),(199,3,'slm',23),(200,3,'rule_dictionnary_printer',0),(201,3,'problem',1151),(203,3,'itilcategory',23),(204,3,'itiltemplate',23),(205,3,'ticketrecurrent',1),(206,3,'ticketcost',23),(208,3,'changevalidation',1044),(209,3,'ticketvalidation',15376),(290,3,'queuednotification',0),(309,3,'domain',23),(395,3,'project',1151),(419,3,'change',1151),(466,3,'taskcategory',23),(467,3,'netpoint',23),(477,3,'state',23),(564,3,'license',127),(572,3,'line',127),(580,3,'lineoperator',23),(588,3,'devicesimcard_pinpuk',3),(596,3,'certificate',127),(604,3,'datacenter',31),(613,3,'personalization',3),(621,3,'rule_asset',0),(628,3,'global_validation',0),(635,3,'cluster',31),(643,3,'externalevent',1055),(651,3,'dashboard',0),(659,3,'appliance',31),(668,3,'plugin_fusioninventory_menu',0),(676,3,'plugin_fusioninventory_agent',0),(684,3,'plugin_fusioninventory_remotecontrol',0),(692,3,'plugin_fusioninventory_configuration',0),(700,3,'plugin_fusioninventory_task',0),(708,3,'plugin_fusioninventory_wol',0),(716,3,'plugin_fusioninventory_group',0),(724,3,'plugin_fusioninventory_collect',0),(732,3,'plugin_fusioninventory_iprange',0),(740,3,'plugin_fusioninventory_credential',0),(748,3,'plugin_fusioninventory_credentialip',0),(756,3,'plugin_fusioninventory_esx',0),(764,3,'plugin_fusioninventory_configsecurity',0),(772,3,'plugin_fusioninventory_networkequipment',0),(780,3,'plugin_fusioninventory_printer',0),(788,3,'plugin_fusioninventory_unmanaged',0),(796,3,'plugin_fusioninventory_importxml',0),(804,3,'plugin_fusioninventory_reportprinter',0),(812,3,'plugin_fusioninventory_reportnetworkequipment',0),(820,3,'plugin_fusioninventory_lock',0),(828,3,'plugin_fusioninventory_ruleimport',0),(836,3,'plugin_fusioninventory_ruleentity',0),(844,3,'plugin_fusioninventory_rulelocation',0),(852,3,'plugin_fusioninventory_blacklist',0),(860,3,'plugin_fusioninventory_rulecollect',0),(868,3,'plugin_fusioninventory_ignoredimportdevice',0),(876,3,'plugin_fusioninventory_package',0),(884,3,'plugin_fusioninventory_userinteractiontemplate',0),(892,3,'plugin_fusioninventory_deploymirror',0),(900,3,'plugin_fusioninventory_selfpackage',0),(910,3,'plugin_moreticket',0),(918,3,'plugin_moreticket_justification',0),(930,3,'plugin_pdf',0),(939,3,'plugin_satisfaction',0),(948,3,'plugin_tag_tag',0),(956,3,'plugin_tasklists',0),(964,3,'plugin_tasklists_see_all',0),(972,3,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:16:14
