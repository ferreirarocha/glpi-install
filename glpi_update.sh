# Como atualizar o GLPI
# bash glpi-install/glpi_update.sh suporte.sinergicait.com.br 123456


FQDN=$1
GLPI_VERSION=$2

# Atualizando o fuso horário
timedatectl set-timezone America/Sao_Paulo



#Backup
su - backupadmin -c " /bin/bash /opt/glpiBackup.sh lite"



# Obtendo o GLPI
  wget https://github.com/glpi-project/glpi/releases/download/"$GLPI_VERSION"/glpi-"$GLPI_VERSION".tgz
  tar xfz glpi-"$GLPI_VERSION".tgz
  rsync -Pav glpi/* /var/www/html/"$FQDN"
  chown www-data. -Rf /var/www/html/"$FQDN"
  mkdir /var/lib/glpi -p  # Diretorio dados glpi
  mkdir /etc/glpi         # Diretório de configuração do GLPI
  mkdir -p /var/log/glpi  # Diretório de logs do GLPI
  chown www-data. -Rf /var/lib/glpi
  chown www-data. -Rf /var/log/glpi
  chown www-data. -Rf /etc/glpi/


php /var/www/html/"$FQDN"/bin/console glpi:database:update   --verbose  -n

rm -rf /var/www/html/"$FQDN"/install/install.php
