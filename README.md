# Install GLPi
Este script de instalação do GLPI é compatível com o ubuntu 20.04
Ele resolve as dependências do GLPI e fornece por padrão
- Scripts de backup configurado
- Configuração de ações automáticas para executarem via systemd
- Modelos de Formulários para o formcreator



# Instalar o git
````
apt install  git -y
````

## Clonar o repositório
````
git clone https://gitlab.com/ferreirarocha/glpi-install.git
````

## Executar o script de instalação
````
bash glpi-install/setup.sh  servicedesk.empresa.com.br 123456 9.5.5
````

## Atualizando o GLPI
````
bash glpi-install/glpi_update.sh  servicedesk.empresa.com.br 9.5.6
````

# Template de formulários para o FormCreator

![template_formcreator](img/template_formcreator.png)


[Baixar Template 1](https://gitlab.com/ferreirarocha/glpi-install/-/raw/master/plugins/formcreator/templates/empresa1.json?inline=false)

# Backup
Backup de arquuivo, banco de dados e configuração do ambiente, preparado para realizar envio pra storages externos com o rclone

```
systemctl  start glpiBackup
```
![Backup](img/backupglpi.png)

## Ações automáticas
As açoes automáticas estão programadas para executar via CLI com o systemctl  list-timers

```
systemctl  list-timers
```

![systemctl  list-timers](img/systemctl-list-timers.png)
