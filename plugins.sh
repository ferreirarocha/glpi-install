mkdir plugins ; cd plugins
dirplugin=$1

# Plugin Ordem de serviço
wget https://github.com/juniormarcati/glpi_os/archive/0.1.2.zip
unzip 0.1.2.zip  -d os
rm 0.1.2.zip

# Plugin FusionInventory
wget https://github.com/fusioninventory/fusioninventory-for-glpi/releases/download/glpi9.5.0%2B1.0/fusioninventory-9.5.0+1.0.tar.bz2
tar jxfv fusioninventory-9.5.0+1.0.tar.bz2
rm -rf fusioninventory-9.5.0+1.0.tar.bz2

# Plugin Metademands
wget https://github.com/InfotelGLPI/metademands/releases/download/2.7.4/glpi-metademands-2.7.4.tar.gz
tar xvf  glpi-metademands-2.7.4.tar.gz
rm glpi-metademands-2.7.4.tar.gz

#TaskLists
wget https://github.com/InfotelGLPI/tasklists/releases/download/1.6.1/glpi-tasklists-1.6.1.tar.gz
tar xvf  glpi-tasklists-1.6.1.tar.gz
rm glpi-tasklists-1.6.1.tar.gz

# Plugin Actual Time
wget https://github.com/ticgal/actualtime/releases/download/1.4.0/glpi-actualtime-1.4.0.tar.tgz
tar  xfz glpi-actualtime-1.4.0.tar.tgz
rm  glpi-actualtime-1.4.0.tar.tgz

# Plugin Satisfaction
wget https://github.com/pluginsGLPI/satisfaction/releases/download/1.5.0/glpi-satisfaction-1.5.0.tar.gz
tar xvf  glpi-satisfaction-1.5.0.tar.gz
rm glpi-satisfaction-1.5.0.tar.gz

# GLP Tag
wget https://github.com/pluginsGLPI/tag/releases/download/2.8.0/glpi-tag-2.8.0.tar.bz2
tar xvf  glpi-tag-2.8.0.tar.bz2
rm glpi-tag-2.8.0.tar.bz2

# GLPI PDF
wget https://forge.glpi-project.org/attachments/download/2314/glpi-pdf-1.7.0.tar.gz
tar xvf  glpi-pdf-1.7.0.tar.gz
rm glpi-pdf-1.7.0.tar.gz

# Plugin Behaviors
wget https://forge.glpi-project.org/attachments/download/2316/glpi-behaviors-2.4.1.tar.gz
tar  xvzf glpi-behaviors-2.4.1.tar.gz
rm glpi-behaviors-2.4.1.tar.gz

# Plugin FormCreator
wget https://github.com/pluginsGLPI/formcreator/releases/download/v2.10.1/glpi-formcreator-2.10.1.tar.bz2
tar jxvf glpi-formcreator-2.10.1.tar.bz2
rm glpi-formcreator-2.10.1.tar.bz2

# Plugin News
wget https://github.com/pluginsGLPI/news/releases/download/1.8.0/glpi-news-1.8.0.tar.bz2
tar xvf  glpi-news-1.8.0.tar.bz2
rm glpi-news-1.8.0.tar.bz2

# Plugin Escalade
wget https://github.com/pluginsGLPI/escalade/releases/download/2.6.0/glpi-escalade-2.6.0.tar.bz2
tar jxvf glpi-escalade-2.6.0.tar.bz2
rm glpi-escalade-2.6.0.tar.bz2

# Plugin Fields
wget https://github.com/pluginsGLPI/fields/releases/download/1.12.0/glpi-fields-1.12.0.tar.bz2
tar jxvf glpi-fields-1.12.0.tar.bz2
rm glpi-fields-1.12.0.tar.bz2


# Data Injection
wget https://github.com/pluginsGLPI/datainjection/releases/download/2.9.0/glpi-datainjection-2.9.0.tar.bz2
tar jxf glpi-datainjection-2.9.0.tar.bz2

# More ticket
wget https://github.com/InfotelGLPI/moreticket/releases/download/1.6.1/glpi-moreticket-1.6.1.tar.gz
tar  xvfz glpi-moreticket-1.6.1.tar.gz

# Movendo os plugins para o diretório  plugins do GLPI
mv ../plugins/*  /var/www/html/$dirplugin/plugins/

# Instalando os plugins 
/var/www/html/$dirplugin/bin/console  glpi:plugin:install  --all -u glpi

# Ativando os plugins 
/var/www/html/$dirplugin/bin/console  glpi:plugin:activate --all


